// Copyright (C) 2016 Mildef.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#[cfg(test)]
mod testBuilder {
    use smac_libs::graphs::simple_graph::SimpleGraphBuilder;

    #[test]
    fn new_graph() -> () {
        let graph = SimpleGraphBuilder::new().build();
        assert_eq!(graph.nodes_size(), 0);
    }

    #[test]
    fn add_broken_link() -> () {
        let graph = SimpleGraphBuilder::new().link(0, 1).build();
        assert!(graph.edges_size() == 0);
    }

    #[test]
    fn add_node() {
        let graph = SimpleGraphBuilder::new()
                        .add_node()
                        .add_node()
                        .build();
        assert_eq!(graph.nodes_size(), 2);
    }

    #[test]
    fn add_link() -> () {
        let graph = SimpleGraphBuilder::new()
                        .add_node()
                        .add_node()
                        .link(0, 1)
                        .build();
        assert_eq!(graph.is_linked(0, 1), true);
    }

    #[test]
    fn remove_link() -> () {
        let graph = SimpleGraphBuilder::new()
                        .add_node()
                        .add_node()
                        .add_node()
                        .link(0, 1)
                        .remove_link(0, 1)
                        .build();
        assert_eq!(graph.is_linked(0, 1), false);
    }

    #[test]
    fn remove_broken_link() -> () {
        let graph = SimpleGraphBuilder::new().remove_link(0, 1).build();
        assert!(graph.edges_size() == 0);
    }

    #[test]
    fn new_from_old() {
        let graph1 = SimpleGraphBuilder::new()
                         .add_node()
                         .add_node()
                         .add_node()
                         .link(0, 1)
                         .build();
        let graph2 = SimpleGraphBuilder::new_from_old(&graph1)
                         .add_node()
                         .build();
        assert_eq!(graph1.nodes_size() + 1, graph2.nodes_size());
    }
}

#[cfg(test)]
mod testSimpleGraph {}
