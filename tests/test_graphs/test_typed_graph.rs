// Copyright (C) 2016 Mildef.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#[cfg(test)]
mod testBuilder {
    use smac_libs::graphs::typed_graph::TypedGraph;
    use smac_libs::graphs::typed_graph::TypedGraphBuilder;
    use std::collections::HashSet;

    // TODO : do corner cases tests

    #[test]
    fn new() {
        let graph: TypedGraph<()> = TypedGraphBuilder::new().build_cloning();
        assert!(graph.nodes_size() == 0);
    }

    #[test]
    fn add_node() {
        let x = 42;
        let graph: TypedGraph<i32> = TypedGraphBuilder::new()
                                         .add_node(x)
                                         .build_cloning();
        assert_eq!(graph.nodes_size(), 1);
        assert_eq!(graph.contains_node(&42), true);
    }

    #[test]
    fn add_node_with_neighbours() {
        let x = 42;
        let y = 32;
        let k = 15;
        let neighbours = vec![x, y];
        let graph = TypedGraphBuilder::new()
                        .add_node_with_neighbours(k, &neighbours)
                        .build_cloning();
        assert!(graph.neighbours(&k).unwrap().len() == 2);
        assert!(graph.contains_node(&x));
        assert!(graph.contains_node(&y));
        assert!(graph.contains_node(&k));
    }

    #[test]
    fn link() {
        let x = 42;
        let y = 32;
        let k = 15;
        let graph = TypedGraphBuilder::new()
                        .add_node(&x)
                        .add_node(&y)
                        .add_node(&k)
                        .link(&&x, &&y)
                        .build_cloning();
        assert!(graph.is_linked(&&x, &&y));
        assert!(graph.contains_node(&&y));
    }

    #[test]
    fn new_from_old() {
        let x = 42;
        let y = 32;
        let k = 15;
        let graph = TypedGraphBuilder::new()
                        .add_node(&x)
                        .add_node(&y)
                        .add_node(&k)
                        .link(&&x, &&y)
                        .build_cloning();
        let second_graph = TypedGraphBuilder::new_from_old(&graph)
                                                .build_cloning();

        assert!(second_graph.is_linked(&&x, &&y));
        assert!(second_graph.nodes_size() == 3);
    }

    #[test]
    fn link_both_ways() {
        let x = 42;
        let y = 32;
        let k = 15;
        let graph = TypedGraphBuilder::new()
                        .add_node(&x)
                        .add_node(&y)
                        .add_node(&k)
                        .link_both_ways(&&x, &&y)
                        .build_cloning();
        assert!(graph.is_linked_both_ways(&&x, &&y));
        assert!(graph.contains_node(&&x));
        assert!(graph.contains_node(&&y));
    }

    #[test]
    fn is_directed_graph() {
        let x = 42;
        let y = 32;
        let k = 15;
        let graph = TypedGraphBuilder::new()
                        .add_node(&x)
                        .add_node(&y)
                        .link_both_ways(&&x, &&y)
                        .build_cloning();
        assert!(!graph.is_directed_graph());
        let graph = TypedGraphBuilder::new_from_old(&graph)
                        .add_node(&k)
                        .build_cloning();
        assert!(graph.is_directed_graph());
    }

    #[test]
    fn remove_node() {
        let x = 42;
        let y = 32;
        let k = 15;
        let graph = TypedGraphBuilder::new()
                        .add_node(&x)
                        .add_node(&y)
                        .add_node(&k)
                        .build_cloning();
        assert!(graph.nodes_size() == 3);
        let graph = TypedGraphBuilder::new_from_old(&graph)
                        .remove_node(&&k)
                        .build_cloning();
        assert!(graph.nodes_size() == 2);
    }

    #[test]
    fn complete_graph() {
        let x = 42;
        let y = 32;
        let k = 15;
        let graph = TypedGraphBuilder::new()
                        .add_node(&x)
                        .add_node(&y)
                        .add_node(&k)
                        .complete_graph()
                        .build_cloning();
        assert!(graph.links_size() == 3 * 3);
    }

    #[test]
    fn complete_graph_no_self_link() {
        let x = 42;
        let y = 32;
        let k = 15;
        let graph = TypedGraphBuilder::new()
                        .add_node(&x)
                        .add_node(&y)
                        .add_node(&k)
                        .complete_graph_no_self_link()
                        .build_cloning();
        assert!(graph.links_size() == 3 * 2);
    }

    #[test]
    fn link_to_all() {
        let x = 42;
        let y = 32;
        let k = 15;
        let graph = TypedGraphBuilder::new()
                        .add_node(&x)
                        .add_node(&y)
                        .add_node(&k)
                        .link_to_all(&&k)
                        .build_cloning();
        assert!(graph.is_linked(&&k, &&k));
        assert!(graph.is_linked(&&k, &&x));
        assert!(graph.is_linked(&&k, &&y));
    }

    #[test]
    fn link_to_all_no_self_link() {
        let x = 42;
        let y = 32;
        let k = 15;
        let graph = TypedGraphBuilder::new()
                        .add_node(&x)
                        .add_node(&y)
                        .add_node(&k)
                        .link_to_all_no_self_link(&&k)
                        .build_cloning();
        assert!(!graph.is_linked(&&k, &&k));
        assert!(graph.is_linked(&&k, &&x));
        assert!(graph.is_linked(&&k, &&y));
    }

    #[test]
    fn link_from_all() {
        let x = 42;
        let y = 32;
        let k = 15;
        let graph = TypedGraphBuilder::new()
                        .add_node(&x)
                        .add_node(&y)
                        .add_node(&k)
                        .link_from_all(&&k)
                        .build_cloning();
        assert!(graph.is_linked(&&k, &&k));
        assert!(graph.is_linked(&&x, &&k));
        assert!(graph.is_linked(&&y, &&k));
    }

    #[test]
    fn link_from_all_no_self_link() {
        let x = 42;
        let y = 32;
        let k = 15;
        let graph = TypedGraphBuilder::new()
                        .add_node(&x)
                        .add_node(&y)
                        .add_node(&k)
                        .link_from_all_no_self_link(&&k)
                        .build_cloning();
        assert!(!graph.is_linked(&&k, &&k));
        assert!(graph.is_linked(&&x, &&k));
        assert!(graph.is_linked(&&y, &&k));
    }

    #[test]
    fn isolate_node() {
        let x = 42;
        let y = 32;
        let k = 15;
        let graph = TypedGraphBuilder::new()
                        .add_node(&x)
                        .add_node(&y)
                        .add_node(&k)
                        .link_to_all(&&k)
                        .link_to_all(&&y)
                        .isolate_node(&&k)
                        .build_cloning();
        assert!(graph.neighbours(&&k).unwrap().len() == 0);
        assert!(!graph.neighbours(&&y).unwrap().contains(&&k));
    }

    #[test]
    fn remove_link() {
        let x = 42;
        let y = 32;
        let k = 15;
        let graph = TypedGraphBuilder::new()
                        .add_node(&x)
                        .add_node(&y)
                        .add_node(&k)
                        .link_to_all(&&k)
                        .link_to_all(&&y)
                        .remove_link(&&k, &&y)
                        .build_cloning();
        assert!(!graph.neighbours(&&k).unwrap().contains(&&y));
        assert!(graph.neighbours(&&y).unwrap().contains(&&k));
    }
}
