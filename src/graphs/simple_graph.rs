// Copyright (C) 2016 Mildef.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! A Graph is a helpful data structure that makes it simple to understand
//! how objects are connected.
//! This implementation is a simple numerical version of a digraph. Each node
//! is represented by a positive integer. The graph is created using a builder
//! so the final data structure is immutable.
//! Because of it's simplicty, the idea is for the user to have a parallel data structure mapping the
//! integer value of the node to an actual object.
//! If the user wants a more complex data structure that keeps track of the objects, look for `TypedDigraph`.

/// A structure representing a simple graph, where nodes and edges don't have a type.
///
/// The SimpleGraph structure has a matrix `neighbours` of `bool` size,
/// such that if `neighbours[i][j] == true`, then `i` is linked to `j`.
/// Note that it's not guaranteed that `neighbours[i][j] == neighbours[j][i]`.
#[derive(Debug)]
pub struct SimpleGraph {
    neighbours: Box<[Box<[bool]>]>,
    is_directed: bool,
    nodes_size: usize,
    links_size: usize,
}

impl SimpleGraph {
    /// If `node1` and `note2` are linked in that direction, returns true. Returns false otherwise.
    ///
    /// # Examples
    /// ``` rust
    /// use smac_libs::graphs::simple_graph::{SimpleGraph, SimpleGraphBuilder};
    ///
    /// let x = SimpleGraphBuilder::new()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .link(0, 1)
    ///                     .build();
    ///
    /// assert!(x.is_linked(0, 1));
    /// assert!(!x.is_linked(1, 0));
    /// ```
    pub fn is_linked(&self, node1: usize, node2: usize) -> bool {
        if node1 > self.neighbours.len() || node2 > self.neighbours.len() {
            return false;
        }
        self.neighbours[node1][node2]
    }

    /// If `node1` and `note2` are linked in both direction, returns true. Returns false otherwise.
    ///
    /// # Examples
    /// ``` rust
    /// use smac_libs::graphs::simple_graph::{SimpleGraph, SimpleGraphBuilder};
    ///
    /// let x = SimpleGraphBuilder::new()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .link_both_ways(0, 1)
    ///                     .build();
    ///
    /// assert!(x.is_linked(0, 1));
    /// assert!(x.is_linked(1, 0));
    /// ```
    pub fn is_linked_both_ways(&self, node1: usize, node2: usize) -> bool {
        if node1 > self.neighbours.len() || node2 > self.neighbours.len() {
            return false;
        }
        self.neighbours[node1][node2] && self.neighbours[node2][node1]
    }

    /// Returns true if the graph is directed, false otherwise.
    ///
    /// A directed graph is a data structure such that, for some
    /// pair `(node1, node2)` in a graph `g`, `g.is_linked_both_ways(node1, node2) == false`
    ///
    /// Notice that this is not valid for `node1 == node2`
    ///
    /// # Examples
    /// ``` rust
    /// use smac_libs::graphs::simple_graph::{SimpleGraph, SimpleGraphBuilder};
    ///
    /// let x = SimpleGraphBuilder::new()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .complete_graph()
    ///                     .build();
    ///
    /// assert!(x.is_directed_graph());
    ///
    /// let x = SimpleGraphBuilder::new()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .complete_graph_no_self_links()
    ///                     .build();
    ///
    /// assert!(x.is_directed_graph());
    /// ```
    pub fn is_directed_graph(&self) -> bool {
        self.is_directed
    }


    /// Number of nodes on the graph.
    ///
    /// # Examples
    /// ``` rust
    /// use smac_libs::graphs::simple_graph::{SimpleGraph, SimpleGraphBuilder};
    ///
    /// let x = SimpleGraphBuilder::new()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .build();
    ///
    /// assert!(x.nodes_size() == 2);
    /// ```
    pub fn nodes_size(&self) -> usize {
        self.nodes_size
    }

    /// Number of edges in the graph.
    ///
    /// Notice that if you want to treat the graph as undircted, the effective number of nodes is
    /// `edges_size / 2`.
    /// # Examples
    /// ``` rust
    /// use smac_libs::graphs::simple_graph::{SimpleGraph, SimpleGraphBuilder};
    ///
    /// let x = SimpleGraphBuilder::new()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .link(0, 1)
    ///                     .build();
    ///
    /// assert!(x.edges_size() == 1);
    ///
    /// let x = SimpleGraphBuilder::new_from_old(&x)
    ///                     .remove_link(0, 1)
    ///                     .link_both_ways(0, 1)
    ///                     .build();
    ///
    /// assert!(x.edges_size() == 2);
    /// ```
    pub fn edges_size(&self) -> usize {
        self.links_size
    }

    /// Returns true if there are at least `node` nodes in the graph.
    ///
    /// # Examples
    /// ``` rust
    /// use smac_libs::graphs::simple_graph::{SimpleGraph, SimpleGraphBuilder};
    ///
    /// let x = SimpleGraphBuilder::new()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .build();
    ///
    /// assert!(x.contains_node(1));
    /// assert!(x.contains_node(0));
    /// assert!(!x.contains_node(3));
    /// ```
    pub fn contains_node(&self, node: usize) -> bool {
        node <= self.neighbours.len()
    }
}

/// Structure of which the the builder pattern will be applied for the creation of `SimpleGraph`s.
pub struct SimpleGraphBuilder {
    neighbours: Vec<Vec<bool>>,
}

/// SimpleGraphBuilder implements the builder pattern for SimpleGraphs.
impl SimpleGraphBuilder {
    /// Starts the graph builder with an empty graph.
    pub fn new() -> SimpleGraphBuilder {
        SimpleGraphBuilder { neighbours: Vec::new() }
    }

    /// Starts the graph builder with a preexisting graph.
    ///
    /// # Examples
    /// ``` rust
    /// use smac_libs::graphs::simple_graph::{SimpleGraph, SimpleGraphBuilder};
    ///
    /// let x = SimpleGraphBuilder::new()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .link(0, 1)
    ///                     .build();
    ///
    /// let g = SimpleGraphBuilder::new_from_old(&x)
    ///                                  .build();
    ///
    /// assert_eq!(g.nodes_size(), x.nodes_size());
    /// assert_eq!(g.edges_size(), x.edges_size());
    /// ```
    pub fn new_from_old(old_graph: &SimpleGraph) -> SimpleGraphBuilder {
        let mut temp_neighbours: Vec<Vec<bool>> = Vec::new();
        for data in old_graph.neighbours.iter() {
            temp_neighbours.push(data.to_vec());
        }
        SimpleGraphBuilder { neighbours: temp_neighbours }
    }

    /// Adds a node to the graph.
    pub fn add_node(&mut self) -> &mut SimpleGraphBuilder {
        // updates the other nodes
        for node in self.neighbours.iter_mut() {
            node.push(false);
        }
        let new_node_vec: Vec<bool> = vec![false; self.neighbours.len() + 1];
        self.neighbours.push(new_node_vec);
        self
    }

    /// Connects every node to every other node in the graph, including themselves.
    ///
    /// # Examples
    /// ``` rust
    /// use smac_libs::graphs::simple_graph::{SimpleGraph, SimpleGraphBuilder};
    ///
    /// let x = SimpleGraphBuilder::new()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .complete_graph()
    ///                     .build();
    ///
    /// assert!(x.edges_size() == 3 * 3);
    /// ```
    pub fn complete_graph(&mut self) -> &mut SimpleGraphBuilder {
        let mut count: usize = 0;
        let limit = self.neighbours.len();
        while count < limit {
            self.link_to_all(count);
            count += 1;
        }
        self
    }

    /// Connects every node to every other node in the graph, except themselves.
    ///
    /// # Examples
    /// ``` rust
    /// use smac_libs::graphs::simple_graph::{SimpleGraph, SimpleGraphBuilder};
    ///
    /// let x = SimpleGraphBuilder::new()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .complete_graph_no_self_links()
    ///                     .build();
    ///
    /// assert!(x.edges_size() == 2 * 3);
    /// ```
    pub fn complete_graph_no_self_links(&mut self) -> &mut SimpleGraphBuilder {
        let mut count: usize = 0;
        let limit = self.neighbours.len();
        while count < limit {
            self.link_to_all_no_self_link(count);
            count += 1;
        }
        self
    }

    /// Connects a preexisting node to every other node in the graph, including itself.
    ///
    /// If the node doesn't exist nothing happens.
    ///
    /// # Examples
    /// ``` rust
    /// use smac_libs::graphs::simple_graph::{SimpleGraph, SimpleGraphBuilder};
    ///
    /// let x = SimpleGraphBuilder::new()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .link_to_all(0)
    ///                     .build();
    ///
    /// assert!(x.edges_size() == 3);
    /// assert!(x.is_linked(0, 1));
    /// assert!(x.is_linked(0, 2));
    /// assert!(x.is_linked(0, 0));
    /// ```
    pub fn link_to_all(&mut self, node: usize) -> &mut SimpleGraphBuilder {
        if node > self.neighbours.len() {
            return self;
        }
        for n in &mut self.neighbours.get_mut(node) {
            for n2 in n.iter_mut() {
                *n2 = true;

            }
        }
        self
    }


    /// Connects a preexisting node to every other node in the graph, except itself.
    ///
    /// If the node doesn't exist nothing happens.
    ///
    /// # Examples
    /// ``` rust
    /// use smac_libs::graphs::simple_graph::{SimpleGraph, SimpleGraphBuilder};
    ///
    /// let x = SimpleGraphBuilder::new()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .link_to_all_no_self_link(0)
    ///                     .build();
    ///
    /// assert!(x.edges_size() == 2);
    /// assert!(x.is_linked(0, 1));
    /// assert!(x.is_linked(0, 2));
    /// assert!(!x.is_linked(0, 0));
    /// ```
    pub fn link_to_all_no_self_link(&mut self, node: usize) -> &mut SimpleGraphBuilder {
        if node > self.neighbours.len() {
            return self;
        }
        for n in &mut self.neighbours.get_mut(node) {
            let mut count: usize = 0;
            for n2 in n.iter_mut() {
                if count == node {
                    count += 1;
                    continue;
                }
                *n2 = true;
                count += 1;
            }
        }
        self
    }

    /// Connects every node to a preexisting node in the graph, including themselves.
    ///
    /// If the node doesn't exist nothing happens.
    ///
    /// # Examples
    /// ``` rust
    /// use smac_libs::graphs::simple_graph::{SimpleGraph, SimpleGraphBuilder};
    ///
    /// let x = SimpleGraphBuilder::new()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .link_from_all(0)
    ///                     .build();
    ///
    /// assert!(x.edges_size() == 3);
    /// assert!(x.is_linked(1, 0));
    /// assert!(x.is_linked(2, 0));
    /// assert!(x.is_linked(0, 0));
    /// ```
    pub fn link_from_all(&mut self, node: usize) -> &mut SimpleGraphBuilder {
        if node > self.neighbours.len() {
            return self;
        }
        for n in &mut self.neighbours {
            n[node] = true;
        }
        self
    }

    /// Connects every node to a preexisting node in the graph, except themselves.
    ///
    /// If the node doesn't exist nothing happens.
    ///
    /// # Examples
    /// ``` rust
    /// use smac_libs::graphs::simple_graph::{SimpleGraph, SimpleGraphBuilder};
    ///
    /// let x = SimpleGraphBuilder::new()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .link_from_all_no_self_link(0)
    ///                     .build();
    ///
    /// assert!(x.edges_size() == 2);
    /// assert!(x.is_linked(1, 0));
    /// assert!(x.is_linked(2, 0));
    /// assert!(!x.is_linked(0, 0));
    /// ```
    pub fn link_from_all_no_self_link(&mut self, node: usize) -> &mut SimpleGraphBuilder {
        if node > self.neighbours.len() {
            return self;
        }
        let mut count: usize = 0;
        for n in &mut self.neighbours {
            if count == node {
                count += 1;
                continue;
            }
            n[node] = true;
            count += 1;
        }
        self
    }

    /// Isolates a node in the graph.
    ///
    /// Removes every link to and from that node.
    /// If the node doesn't exist nothing happens.
    ///
    /// # Examples
    /// ``` rust
    /// use smac_libs::graphs::simple_graph::{SimpleGraph, SimpleGraphBuilder};
    ///
    /// let x = SimpleGraphBuilder::new()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .complete_graph()
    ///                     .isolate_node(0)
    ///                     .build();
    ///
    /// assert!(!x.is_linked(0, 1));
    /// assert!(!x.is_linked(0, 2));
    /// assert!(!x.is_linked(0, 0));
    /// ```
    pub fn isolate_node(&mut self, node: usize) -> &mut SimpleGraphBuilder {
        if node > self.neighbours.len() {
            return self;
        }
        for n in &mut self.neighbours {
            n[node] = false;
        }
        for n in self.neighbours.get_mut(node) {
            for n2 in n.iter_mut() {
                *n2 = false;
            }
        }
        self
    }

    /// Empties the graph.
    ///
    /// Removes every link and node from the graph.
    ///
    /// # Examples
    /// ``` rust
    /// use smac_libs::graphs::simple_graph::{SimpleGraph, SimpleGraphBuilder};
    ///
    /// let x = SimpleGraphBuilder::new()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .complete_graph()
    ///                     .make_empty()
    ///                     .build();
    ///
    /// assert!(x.nodes_size() == 0);
    /// assert!(x.edges_size() == 0);
    /// ```
    pub fn make_empty(&mut self) -> &mut SimpleGraphBuilder {
        self.neighbours.clear();
        self
    }



    /// Links `node1` to `node2` and `node2` to `node1` in the graph.
    ///
    /// If `node1` or `node2` doesn't exit in the graph nothing happens.
    ///
    /// # Examples
    /// ``` rust
    /// use smac_libs::graphs::simple_graph::{SimpleGraph, SimpleGraphBuilder};
    ///
    /// let x = SimpleGraphBuilder::new()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .link_both_ways(0, 1)
    ///                     .build();
    ///
    /// assert!(x.is_linked_both_ways(0, 1));
    /// assert!(x.is_linked(0, 1));
    /// assert!(x.is_linked(1, 0));
    /// ```
    pub fn link_both_ways(&mut self, node1: usize, node2: usize) -> &mut SimpleGraphBuilder {
        if node1 > self.neighbours.len() || node2 > self.neighbours.len() {
            return self;
        }
        self.neighbours[node1][node2] = true;
        self.neighbours[node2][node1] = true;
        self
    }

    /// Links `node1` to `node2` in the graph.
    ///
    /// If `node1` or `node2` doesn't exit in the graph nothing happens.
    ///
    /// # Examples
    /// ``` rust
    /// use smac_libs::graphs::simple_graph::{SimpleGraph, SimpleGraphBuilder};
    ///
    /// let x = SimpleGraphBuilder::new()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .link(0, 1)
    ///                     .build();
    ///
    /// assert!(x.is_linked(0, 1));
    /// assert!(!x.is_linked(1, 0));
    /// ```
    pub fn link(&mut self, node1: usize, node2: usize) -> &mut SimpleGraphBuilder {
        if node1 > self.neighbours.len() || node2 > self.neighbours.len() {
            return self;
        }
        self.neighbours[node1][node2] = true;
        self
    }

    /// Remove `node` from the graph.
    ///
    /// This operation "shifts" every node value to the left. Be careful.
    /// If `node` doesn't exit in the graph nothing happens.
    ///
    /// # Examples
    /// ``` rust
    /// use smac_libs::graphs::simple_graph::{SimpleGraph, SimpleGraphBuilder};
    ///
    /// let x = SimpleGraphBuilder::new()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .remove_node(0)
    ///                     .build();
    ///
    /// assert!(x.nodes_size() == 2);
    /// ```
    pub fn remove_node(&mut self, node: usize) -> &mut SimpleGraphBuilder {
        if node > self.neighbours.len() {
            return self;
        }
        self.neighbours.remove(node);
        for n in &mut self.neighbours {
            n.remove(node);
        }
        self
    }


    /// Remves the link from `node1` to `node2` in the graph.
    ///
    /// If `node1` or `node2` doesn't exist in the graph nothing happens.
    /// If `node' isn't linked to `node2` nothing happens.
    ///
    /// # Examples
    /// ``` rust
    /// use smac_libs::graphs::simple_graph::{SimpleGraph, SimpleGraphBuilder};
    ///
    /// let x = SimpleGraphBuilder::new()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .complete_graph()
    ///                     .remove_link(0, 1)
    ///                     .build();
    ///
    /// assert!(!x.is_linked(0, 1));
    /// assert!(x.is_linked(1, 0));
    /// ```
    pub fn remove_link(&mut self, node1: usize, node2: usize) -> &mut SimpleGraphBuilder {
        if node1 > self.neighbours.len() || node2 > self.neighbours.len() {
            return self;
        }
        self.neighbours[node1][node2] = false;
        self
    }

    /// Remves the links from `node1` to `node2` and from `node2` to `node1` in the graph.
    ///
    /// If `node1` or `node2` doesn't exist in the graph nothing happens.
    /// Will only remove links that exist. If a link doesn't exist that connection won't change.
///
    /// # Examples
    /// ``` rust
    /// use smac_libs::graphs::simple_graph::{SimpleGraph, SimpleGraphBuilder};
    ///
    /// let x = SimpleGraphBuilder::new()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .add_node()
    ///                     .complete_graph()
    ///                     .remove_link_both_ways(0, 1)
    ///                     .build();
    ///
    /// assert!(!x.is_linked(0, 1));
    /// assert!(!x.is_linked(1, 0));
    /// ```
    pub fn remove_link_both_ways(&mut self, node1: usize, node2: usize) -> &mut SimpleGraphBuilder {
        if node1 > self.neighbours.len() || node2 > self.neighbours.len() {
            return self;
        }
        self.neighbours[node1][node2] = false;
        self.neighbours[node2][node1] = false;
        self
    }


    /// Builds the graph.
    pub fn build(&self) -> SimpleGraph {
        SimpleGraph {
            neighbours: self.get_slices(),
            nodes_size: self.neighbours.len(),
            links_size: self.edges_size(),
            is_directed: self.is_directed(),
        }
    }

    #[inline]
    fn get_slices(&self) -> Box<[Box<[bool]>]> {
        let mut temp_neighbour: Vec<Box<[bool]>> = Vec::with_capacity(self.neighbours.len());
        for node_vec in &self.neighbours {
            temp_neighbour.push(node_vec.clone().into_boxed_slice());
        }
        temp_neighbour.into_boxed_slice()
    }

    #[inline]
    fn edges_size(&self) -> usize {
        let mut temp_edges_size = 0;
        for n in self.neighbours.iter().flat_map(|e| e.iter()) {
            if *n {
                temp_edges_size += 1;
            }
        }
        temp_edges_size
    }

    #[inline]
    fn is_directed(&self) -> bool {
        let mut count1: usize = 0;
        for n1 in &self.neighbours {
            let mut count2: usize = 0;
            for n2 in n1 {
                if count1 == count2 {
                    continue;
                }
                // If n2 is connected to n1, checks if n1 is also connected to n2
                // If it can't be satisfied for some pair `(n1, n2)`, then the graph
                // is not directed.
                if *n2 && !self.neighbours
                               .get(count2)
                               .unwrap()
                               .get(count1)
                               .unwrap() {
                    return false
                }
                count2 += 1;
            }
            count1 += 1;
        }
        true
    }
}
