// Copyright (C) 2016 Mildef.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::collections::HashMap;
use std::collections::HashSet;
use std::rc::Rc;
use std::ops::Deref;
use std::hash::Hash;
use std::cmp::Eq;

/// A implementation of a typed graph data structure.
///
/// Different from the `SimpleGraph` implementation, this does not
/// distinguishes between a directed or undirected details, therefore,
/// if the user wants to have a undirected graph (i.e.
/// `is_linked(node1, node2) == is_linked(node2, node1)`) he or she must manually
/// check specifically for that case.
#[derive(Debug)]
pub struct TypedGraph<T>
    where T: Eq + Hash + PartialEq<T>
{
    neighbours: HashMap<Rc<T>, HashSet<Rc<T>>>,
}

impl<T> TypedGraph<T>
    where T: Eq + Hash + PartialEq<T>
{
    /// Returns the number of nodes in the graph.
    pub fn nodes_size(&self) -> usize {
        self.neighbours.len()
    }

    /// Returns `true` if `node1` and `node2` are linked.
    ///
    /// Notice that `is_linked(node1, node2)` is not necessarily means
    /// `is_linked(node2, node1)`.
    pub fn is_linked(&self, node1: &T, node2: &T) -> bool {
        match self.neighbours.get(node1) {
            Some(v) => v.contains(node2),
            None => false,
        }
    }

    /// Returns an `Option` containing a reference to the new_neighbours
    /// of `node`.
    pub fn neighbours(&self, node: &T) -> Option<&HashSet<Rc<T>>> {
        self.neighbours.get(node)
    }

    /// Returns `true` if `node1` is linked to `node2` AND `node2` is linked to `node1`,
    /// and `false`otherwise.
    pub fn is_linked_both_ways(&self, node1: &T, node2: &T) -> bool {
        match self.neighbours.get(node1) {
            Some(set) => {
                match set.contains(node2) {
                    true => {
                        match self.neighbours.get(node2) {
                            Some(set2) => set2.contains(node1),
                            None => false,
                        }
                    }
                    false => false,
                }
            }
            None => false,
        }
    }

    /// Returns `true` if for any pair `(node1, node2)` in the graph
    /// `is_linked_both_ways(node1, node2) == false`, and false otherwise.
    pub fn is_directed_graph(&self) -> bool {
        for node1 in self.neighbours.keys() {
            for node2 in self.neighbours.keys() {
                if node1 == node2 {
                    continue;
                } else {
                    if !self.is_linked_both_ways(node1, node2) {
                        return true;
                    }
                }
            }
        }
        false
    }

    /// Returns the number of links in the graph
    pub fn links_size(&self) -> usize {
        let mut size: usize = 0;
        for (_, values) in self.neighbours.iter() {
            size += values.len();
        }
        size
    }

    pub fn contains_node(&self, node: &T) -> bool {
        self.neighbours.contains_key(node)
    }
}

/// An implementation of the builder pattern for TypedGraph.
pub struct TypedGraphBuilder<T>
    where T: Eq + Hash + PartialEq<T>
{
    nodes: HashSet<Rc<T>>,
    neighbours: HashMap<Rc<T>, HashSet<Rc<T>>>,
}

impl<T> TypedGraphBuilder<T>
    where T: Eq + Hash + PartialEq<T> + Clone
{
    /// Creates a new `TypedGraphBuilder` with zero nodes and links.
    pub fn new() -> TypedGraphBuilder<T> {
        TypedGraphBuilder {
            neighbours: HashMap::new(),
            nodes: HashSet::new(),
        }
    }


    /// Creates a new `TypedGraphBuilder` in the same state as an preexisting graph.
    pub fn new_from_old(old: &TypedGraph<T>) -> TypedGraphBuilder<T> {
        let mut temp_nodes = HashSet::new();
        for (node, _) in old.neighbours.iter() {
            temp_nodes.insert(node.clone());
        }
        TypedGraphBuilder {
            neighbours: old.neighbours.clone(),
            nodes: temp_nodes,
        }
    }

    /// Same as `new_from_old`, but consumes the preexisting graph.
    // pub fn new_from_old_by_move(old: TypedGraph<T>) -> TypedGraphBuilder<T> {
    //     TypedGraphBuilder { neighbours: old.neighbours }
    // }
    /// Adds a node to the graph.
    pub fn add_node(&mut self, node: T) -> &mut TypedGraphBuilder<T> {
        let temp_node = Rc::new(node);
        self.nodes.insert(temp_node.clone());
        self.neighbours.entry(temp_node).or_insert_with(HashSet::new);
        self
    }

    /// Removes a node from the graph.
    ///
    /// The node is first isolated using `isolate_node`, and then removed from the
    /// node set. In other words, the removal should keep the integrity of the graph (
    /// every node that is neighbour of another node is also in the node set).
    pub fn remove_node(&mut self, node: &T) -> &mut TypedGraphBuilder<T> {
        self.isolate_node(node);
        self.neighbours.remove(node);
        self.nodes.remove(node);
        self
    }

    /// Links every pair `(node1, node2)` in the graph, even when `node1 == node2`
    pub fn complete_graph(&mut self) -> &mut TypedGraphBuilder<T> {
        for node in &self.nodes {
            let temp_set = self.get_node_set_cloned();
            self.neighbours.insert(node.clone(), temp_set);
        }
        self
    }

    /// Links every pair `(node1, node2)` in the graph, except when `node1 == node2`
    pub fn complete_graph_no_self_link(&mut self) -> &mut TypedGraphBuilder<T> {
        for node in &self.nodes {
            let mut temp_set = self.get_node_set_cloned();
            temp_set.remove(node);
            self.neighbours.insert(node.clone(), temp_set);
        }
        self
    }


    /// Links `node` to every other node in the graph, including itself.
    ///
    /// If node isn't already in the graph nothing happens.
    /// Because it needs to find every distinct node in the graph and clone the data,
    /// this method might be a slow operation on big graphs.
    pub fn link_to_all(&mut self, node: &T) -> &mut TypedGraphBuilder<T> {
        if !self.nodes.contains(node) {
            return self;
        }
        let new_neighbours = self.get_node_set_cloned();
        let real_node = self.nodes.get(node).unwrap().clone();
        self.neighbours.insert(real_node, new_neighbours);
        self
    }

    /// TODO document add_with_link_to_all function
    pub fn add_with_link_to_all(&mut self, node: T) -> &mut TypedGraphBuilder<T> {
        let temp_node = Rc::new(node);
        let new_neighbours = self.get_node_set_cloned();
        self.neighbours.insert(temp_node, new_neighbours);
        self
    }

    /// Links `node` to every other node in the graph, EXCEPT itself.
    ///
    /// If the node isn't already in the graph nothing happens.
    pub fn link_to_all_no_self_link(&mut self, node: &T) -> &mut TypedGraphBuilder<T> {
        if !self.nodes.contains(node) {
            return self;
        }
        let mut new_neighbours = self.get_node_set_cloned();
        new_neighbours.remove(node);
        let real_node = self.nodes.get(node).unwrap().clone();
        self.neighbours.insert(real_node, new_neighbours);
        self
    }

    /// TODO document add_with_link_to_all_no_self_link function
    pub fn add_with_link_to_all_no_self_link(&mut self, node: T) -> &mut TypedGraphBuilder<T> {
        let temp_node = Rc::new(node);
        let mut new_neighbours = self.get_node_set_cloned();
        new_neighbours.remove(&temp_node);
        self.neighbours.insert(temp_node, new_neighbours);
        self
    }

    /// Links every node in the graph to `node`, including itself.
    ///
    /// If the node isn't already in the graph nothing happens.
    pub fn link_from_all(&mut self, node: &T) -> &mut TypedGraphBuilder<T> {
        if !self.nodes.contains(node) {
            return self;
        }
        for (_, values) in self.neighbours.iter_mut() {
            values.insert(self.nodes.get(node).unwrap().clone());
        }
        self
    }

    /// TODO document add_with_link_from_all function
    pub fn add_with_link_from_all(&mut self, node: T) -> &mut TypedGraphBuilder<T> {
        let temp_node = Rc::new(node);
        self.nodes.insert(temp_node.clone());
        for (_, values) in self.neighbours.iter_mut() {
            values.insert(temp_node.clone());
        }
        self
    }

    /// Links every node in the graph to `node`, EXCEPT itself.
    ///
    /// If the node isn't already in the graph nothing happens.
    pub fn link_from_all_no_self_link(&mut self, node: &T) -> &mut TypedGraphBuilder<T> {
        if !self.nodes.contains(node) {
            return self;
        }
        for (key, values) in self.neighbours.iter_mut() {
            if key.deref() == node {
                continue;
            }
            values.insert(self.nodes.get(node).unwrap().clone());
        }
        self
    }

    /// TODO document add_with_link_from_all_no_self_link function
    pub fn add_with_link_from_all_no_self_link(&mut self, node: T) -> &mut TypedGraphBuilder<T> {
        let temp_node = Rc::new(node);
        self.nodes.insert(temp_node.clone());
        for (key, values) in self.neighbours.iter_mut() {
            if key.deref() == temp_node.deref() {
                continue;
            }
            values.insert(temp_node.clone());
        }
        self
    }


    /// Removes every edge of `node`.
    pub fn isolate_node(&mut self, node: &T) -> &mut TypedGraphBuilder<T> {
        for (_, values) in self.neighbours.iter_mut() {
            values.remove(node);
        }
        match self.neighbours.get_mut(node) {
            Some(set) => {
                set.clear();
            }
            None => (),
        }
        self
    }

    /// TODO document add_neighbours function
    pub fn add_neighbours(&mut self, node: &T, neighbours: &[T]) -> &mut TypedGraphBuilder<T> {
        if !self.node_exists(node) {
            return self
        }
        let temp_node = self.nodes.get(node).unwrap().clone();
        let mut temp_set: HashSet<Rc<T>> = HashSet::new();
        for key in neighbours.iter() {
            let temp_node = Rc::new(key.clone());
            self.nodes.insert(temp_node.clone());
            temp_set.insert(temp_node);
        }
        self.neighbours.insert(temp_node, temp_set);
        self
    }


    /// Adds a node to the graph with a set of neighbours.
    ///
    /// If the node already exists it will replace the neighbours
    /// by the new neighbours. If any of the nodes in `neighbours`
    /// isn't already in the graph, they will be added with an empty neighbourhood.
    pub fn add_node_with_neighbours(&mut self,
                                    node: T,
                                    neighbours: &[T])
                                    -> &mut TypedGraphBuilder<T> {
        let mut temp_set: HashSet<Rc<T>> = HashSet::new();
        for key in neighbours.iter() {
            let temp_node = Rc::new(key.clone());
            self.nodes.insert(temp_node.clone());
            temp_set.insert(temp_node);
        }
        let temp_node = Rc::new(node);
        self.nodes.insert(temp_node.clone());
        self.neighbours.insert(temp_node, temp_set);
        self
    }

    /// Links `node1` to `node2` and vice versa.
    ///
    /// If eighter `node1` or `node2` isn't already in the graph they will be added.
    pub fn add_and_link_both_ways(&mut self, node1: T, node2: T) -> &mut TypedGraphBuilder<T> {
        let temp_node1 = Rc::new(node1);
        let temp_node2 = Rc::new(node2);
        self.nodes.insert(temp_node1.clone());
        self.nodes.insert(temp_node2.clone());
        self.link(temp_node1.deref(), temp_node2.deref())
            .link(temp_node2.deref(), temp_node1.deref())
    }

    /// TODO document link_both_ways function
    pub fn link_both_ways(&mut self, node1: &T, node2: &T) -> &mut TypedGraphBuilder<T> {
        if !self.node_exists(node1) || !self.node_exists(node2) {
            return self
        }
        let temp_node1 = self.nodes.get(node1).unwrap().clone();
        let temp_node2 = self.nodes.get(node2).unwrap().clone();
        self.neighbours.entry(temp_node1.clone())
                            .or_insert_with(HashSet::new)
                            .insert(temp_node2.clone());
        self.neighbours.entry(temp_node2)
                            .or_insert_with(HashSet::new)
                            .insert(temp_node1);
        self
    }

    /// Links `node1` to `node2`.
    ///
    /// If eighter isn't already in the graph they will be added.
    pub fn add_and_link(&mut self, node1: T, node2: T) -> &mut TypedGraphBuilder<T> {
        let temp_node1 = Rc::new(node1);
        let temp_node2 = Rc::new(node2);
        self.nodes.insert(temp_node1.clone());
        self.nodes.insert(temp_node2.clone());
        self.neighbours.entry(temp_node1).or_insert_with(HashSet::new).insert(temp_node2);
        self
    }

    /// TODO document link method
    pub fn link(&mut self, node1: &T, node2: &T) -> &mut TypedGraphBuilder<T> {
        if !self.node_exists(node1) || !self.node_exists(node2) {
            return self
        }
        let temp_node1 = self.nodes.get(node1).unwrap().clone();
        let temp_node2 = self.nodes.get(node2).unwrap().clone();
        self.neighbours.entry(temp_node1).or_insert_with(HashSet::new).insert(temp_node2);
        self
    }


    /// Removes the link between `node1` and `node2`.
    ///
    /// If the link doesn't exists nothing will happen.
    /// If eighter node isn't in the graph nothing will happen.
    pub fn remove_link(&mut self, node1: &T, node2: &T) -> &mut TypedGraphBuilder<T> {
        match self.neighbours.get_mut(node1) {
            Some(set) => {
                set.remove(node2);
            }
            None => (),
        }
        self
    }

    /// TODO document make_empty method
    pub fn make_empty(&mut self) -> &mut TypedGraphBuilder<T> {
        self.nodes.clear();
        self.neighbours.clear();
        self
    }


    /// Builds the final graph.TypedGraph
    ///
    /// This operation DOES NOT consume the builder.
    /// The construction of the final graph is made by a clone of the
    /// internal state of the builder.
    pub fn build_cloning(&self) -> TypedGraph<T> {
        let mut temp_map = HashMap::new();
        for node in &self.nodes {
            let temp_set = match self.neighbours.get(node) {
                Some(n) => n.clone(),
                None => HashSet::new(),
            };

            temp_map.insert(node.clone(), temp_set);
        }
        TypedGraph { neighbours: temp_map }
    }

    pub fn build_moving(mut self) -> TypedGraph<T> {
        let mut temp_map = HashMap::new();
        for node in self.nodes {
            let temp_set = self.neighbours.remove(&node).unwrap_or(HashSet::<Rc<T>>::new());
            temp_map.insert(node.clone(), temp_set);
        }
        TypedGraph { neighbours: self.neighbours.clone() }
    }

    #[inline]
    fn get_node_set_cloned(&self) -> HashSet<Rc<T>> {
        let mut node_set = HashSet::new();
        for (key, values) in self.neighbours.iter() {
            for value in values.iter() {
                node_set.insert(value.clone());
            }
            node_set.insert(key.clone());
        }
        node_set
    }

    #[inline]
    fn node_exists(&self, node: &T) -> bool {
        self.nodes.contains(node)
    }
}

